# Go config generator for jenkins

It should be called when starting up the jenkins server and do the following:

 - Verifies if the configuration is already present at `$JENKINS_HOME` and quit if it does
 - Generate a jenkins configuration based on a configuration file (yaml)
    - Prepare kubernetes configuration if deployed to a kubernetes cluster
    - Based on kubernetes configuration generates jenkins general configuration
    - Generates credentials configuration file
    - Generates Alauda plugin configuration file

## configuration file 

example `assets/config.yaml`

parts:
 - Jenkins
   - Clouds (Kubernetes)
   - Templates
     - Pods
 - Credentials
 - Alauda

### Jenkins

Used for general Jenkins configuration

`version`: (Optional) Jenkins version (by default use `JENKINS_VERSION` env var)

`numExecutors`: (Optional) number of jobs that can run on the master, defaults to 2

`mode`: master jobs execution operation (NORMAL)

`useSecurity`: restrict anonymous access

`securityRealm.disableSignup`: disable signup option, defaults to false

`securityRealm.enableCaptcha`: enable captcha validation on login, defaults to false

`disableRemeberMe`: remember me option on login scree, defaults to false

`scmCheckoutRetryCount`: number of retries for checkout, defaults to 0

`slaveAgentPort`: port to be allocated to slave JNLP communication, defaults to 50000

`label`: label given to the master node

`noUsageStatistics`: disable sending stats to Jenkins main server

#### Clouds (Kubernetes)
`type: kubernetes`

Used to configurate a Kubernetes cluster configuration to schedule slaves

`kubernetes`: main entry point for kubernetes configuration

`name`: name for the k8s cluster

`serverUrl`: Kubernetes API URL. 

if not provided it will:
 - try to search for a `kubernetes` service configuration in the k8s cluster.
 - check for `KUBERNETES_URL` env var,
 - Defaults to `https://kubernetes.default.svc.cluster.local`.

`skipTlsVerify`: ignore invalid server certificates from k8s api server

`serverCertificate`: K8s API server CA certificate. If not provided will look for `serviceaccount` configuration in the container

`namespace`: Kubernetes namespace to be used. 

If not provided will:
 - look for `serviceaccount` configuration in the container
 - use `KUBERNETES_NAMESPACE` env var

`jenkinsUrl`: Jenkins access URL.

If not provided:
 - look for Jenkins service name stored in `JENKINS_SERVICE_NAME` env var
 - use the above service name to fetch configuration from cluster

`jenkinsTunnel`: address that slave nodes use to access jenkins master

If not provided will:
 - look for Jenkins service name stored in `JNLP_SERVICE_NAME` env var
 - use the above service name to fetch configuration from cluster
 - if the service name is equal to `JENKINS_SERVICE_NAME` env var value will look for the `JNLP` port configuration in service
 - will use `JENKINS_SERVICE_NAME` service's configuration with port 50000

`credentialsId`: Credentials for kubernetes node (see Credentials below)

`containerCap`: max number of containers that will be activated, defaults to 30

`retentionTimeout`: max time to retain a container after job execution (see Kubernetes plugin)

`templates`: templates for different kubernetes resources. Currently only supports Pod

##### Templates (pod)

`type: pod`

`pod`: store all pod related configuration:


`inheritFrom`: template to inherit from. Can be used to simplify pod template configuration

`name`: template name. used in Kubernetes DSL

`label`: pod template label. Used for `node` DSL command assign a template

`namespace`: namespace to schedule this pod. Use of the same namespace is recommended

`privileged`: runs privileged pod

`instanceCap`: max number of instances created. Defaults to 2147483647

`connectTimeout`: connection timeout for slave pod. Once timedout, the plugin will schedule a new pod

`idleMinutes`:  idle pod minutes between tasks

`serviceAccount`: use of a specific service account to schedule pod. Defaults to blank

`nodeSelector`: currently not supported

`nodeUsage`: usage as jenkins node, same as Master's configuration. (NORMAL)

`customWorkspaceVolumeEnabled`: enable custom workspace volume

`workspaceVolume`: if `customWorkspaceVolumeEnabled` is enable, this key will store it's configuration

`workspaceVolume.type`: type of volume, currently supports `hostPath` and `persistentVolumeClaim`

`workspaceVolume.hostPath`: when `type: hostPath` this will store its configuration:

 - `path`: host's path. e.g `/data/workspace`

`workspaceVolume.persistentVolumeClaim`: when `type: persistentVolumeClaim` will store its configuration:

 - `claimName`: pvc's name
 - `readOnly`: readonly flag for pvc

`volumes`: will store volume configuration for pod (array)

`volumes.x.type`: volume type, currently supports `hostPath` and `persistentVolumeClaim`

`volumes.x.hostPath`: store configuration for type `hostPath`
 - `moutPath`: container's mount path
 - `hostPath`: host's mount path

`volumes.x.persistentVolumeClaim`: store configuration for type `persistentVolumeClaim`
 - `mountPath`: container's mount path
 - `claimName`: pvc's name
 - `readOnly` : read only flag for pvc

`containers`: list of containers for pod

`containers.x.name`: name of container
`containers.x.image`: image used by container
`containers.x.privileged`:  privileged container flag
`containers.x.alwaysPullImage`: pull image rule
`containers.x.workingDir`: workspace for container
`containers.x.command`: command for container
`containers.x.args`: arguments for container
`containers.x.ttyEnabled`: enables user input in terminal (not advised for jobs)
`containers.x.resourceRequestCpu`: requested CPU number
`containers.x.resourceRquestMemory`: request Memory
`containers.x.resourceLimitCpu`: CPU usage limit
`containers.x.resourceLimitMemory`: Memory usage limit

`imagePullSecrets`: list of secrets to be used when pulling image
 - `name`: secret name in namespace

### Credentials

Used by Kubernetes plugin to automatically create credentials record 

`Attention: Currently not working`


### Alauda

Used by Alauda Pipeline Plugin for configuration

`consoleUrl`: URl to access rubick: e.g `https://enterprise.alauda.cn`

`apiEndpoint`: URL for jakiro: e.g `https://api.alauda.cn`

`apiToken`: User token to be used (obtained after login)

`namespace`: Root Account name

`spaceName`: (Optional) Default space name to be used 


```
jenkins:
  version:
  numExecutors:
  mode: NORMAL
  useSecurity: true
  securityRealm:
    disableSignup: true
    enableCaptcha: false
  disableRememberMe: false
  scmCheckoutRetryCount: 0
  slaveAgentPort: 50000
  label: master
  noUsageStatistics: true
  clouds:
  - type: kubernetes
    kubernetes:
        name: alauda
        serverUrl: 
        skipTlsVerify: true
        serverCertificate: 
        namespace:
        jenkinsUrl: 
        jenkinsTunnel:
        credentialsId: 1a12dfa4-7fc5-47a7-aa17-cc56572a41c7
        containerCap: 10
        retentionTimeout: 5
        templates:
        - type: pod
          pod:
            inheritFrom: in
            name: mave
            label: mave
            namespace: nam
            privileged: false
            instanceCap: 2147483647
            connectTimeout: 100
            idleMinutes: 0
            serviceAccount: aa
            nodeSelector:
            nodeUsage: NORMAL
            customWorkspaceVolumeEnabled: true
            workspaceVolume:
                type: hostPath
                hostPath:
                    path: /data/workspace
                persistentVolumeClaim:
                    claimName: pvcname
                    readOnly: false
            volumes:
            - type: hostPath
              hostPath:
                mountPath:
                hostPath:
              persistentVolumeClaim:
                claimName: pvcname
                readOnly:
                mountPath: 
            containers:
            - name:
              image:
              privileged: false
              alwaysPullImage: true
              workingDir: /tmp/ha
              command:
              args: ${computer.jnlpmac} ${computer.name}
              ttyEnabled: false
              resourceRequestCpu:
              resourceRequestMemory:
              resourceLimitCpu:
              resourceLimitMemory:
            imagePullSecrets:
            - name: secretname

credentials:
- type: kubernetes
  kubernetes:
    scope: GENERAL
    id: 1a12dfa4-7fc5-47a7-aa17-cc56572a41c7
    description: kubernetes credentials

alauda:
  consoleUrl: https://enterprise.alauda.cn
  apiEndpoint: https://api.alauda.cn
  apiToken: 123456
  namespace: alauda
  spaceName: dev-space


            
```


package templates_test

import (
	"testing"
	"devops/jenkins/generator/templates"
	// "reflect"

	"fmt"
)

func TestReadGenericYamlGenerateTemplate(t *testing.T) {
	type Test struct {
		Name string
		File string
		Template string
		Expected string
		Err error
	}

	table := []Test{
		{
			"simple",
			"../assets/generic.yaml",
			`<?xml version='1.0' encoding='UTF-8'?>
			<hello>
				{{ with .some }}
				{{.test}}
				{{ end }}
			</hello>`,
			`<?xml version='1.0' encoding='UTF-8'?>
			<hello>
				
				hello
				
			</hello>`,
			nil,
		},
	}

	for i, tst := range table {
		res, _ := templates.ReadGenericYAML(tst.File)
		fmt.Println("result", res)
		content, _ := templates.Generic(tst.Template, res)
		if content != tst.Expected {
			t.Errorf("[%d] expected is different \"%v\" != \"%v\"", i, content, tst.Expected)
		}
	}
}
package templates_test

import (
	"testing"
	"devops/jenkins/generator/templates"
)

func TestGeneric(t *testing.T) {

	type Test struct {
		Name string
		Template string
		Content interface{}
		Expected string
		Err error
	}

	table := []Test{
		{
			"simple",
			"<?xml version='1.0' encoding='UTF-8'?><hello>{{.Test}}</hello>",
			map[string]string{"Test": "hello world!"},
			"<?xml version='1.0' encoding='UTF-8'?><hello>hello world!</hello>",
			nil,
		},
	}

	for i, tst := range table {
		res, err := templates.Generic(tst.Template, tst.Content)
		if res != tst.Expected {
			t.Errorf("[%d] expected is different \"%s\" != \"%s\"", i, res, tst.Expected)
		}
		if err != tst.Err {
			t.Errorf("[%d] err is different \"%s\" != \"%s\"", i, err, tst.Err)
		}
	}
}
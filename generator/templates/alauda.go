package templates

import (
	"bytes"
)

const (
	alaudaTemp = `<?xml version='1.0' encoding='UTF-8'?>
<io.alauda.jenkins.plugins.pipeline.AlaudaConfiguration plugin="alauda-pipeline@1.0-SNAPSHOT">
	<consoleURL>{{.ConsoleURL}}</consoleURL>
	<apiEndpoint>{{.APIEndpoint}}</apiEndpoint>
	<apiToken>{{.Token}}</apiToken>
	<namespace>{{.Namespace}}</namespace>
	<spaceName>{{.SpaceName}}</spaceName>
	<account>{{.Account}}</account>
	<clusterName>{{.ClusterName}}</clusterName>

</io.alauda.jenkins.plugins.pipeline.AlaudaConfiguration>
`
)

// AlaudaPipeline alauda pipeline struct
type AlaudaPipeline struct {
	ConsoleURL string `mapstructure:"consoleUrl"`
	APIEndpoint string `mapstructure:"apiEndpoint"`
	Account string `mapstructure:"account"`
	Token string `mapstructure:"apiToken"`
	SpaceName string `mapstructure:"spaceName"`
	ClusterName string `mapstructure:"clusterName"`
	Namespace string `mapstructure:"namespace"`

}

// Parse parse template
func (a AlaudaPipeline) Parse() (content string, err error) {
	byt := new(bytes.Buffer)
	if err = Render(alaudaTemp, a, byt); err != nil {
		return
	}
	content = byt.String()
	return
}
package templates

import (
	"bytes"
)

func Generic(template string, content interface{}) (render string, err error) {
	byt := new(bytes.Buffer)
	err  = Render(template, content, byt)
	if err == nil {
		render = byt.String()
	}
	return
}
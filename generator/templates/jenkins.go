package templates

import (
	"bytes"

	"github.com/spf13/viper"
)

const (
	jenkinsTemp = `<?xml version='1.0' encoding='UTF-8'?>
	<hudson>
	<disabledAdministrativeMonitors/>
	<version>{{.Version}}</version>
	<numExecutors>{{.NumExecutors}}</numExecutors>
	<mode>{{if eq .Mode "" }}{{.Mode}}{{else}}NORMAL{{end}}</mode>
	<useSecurity>{{.UseSecurity}}</useSecurity>
	<authorizationStrategy class="hudson.security.FullControlOnceLoggedInAuthorizationStrategy">
    	<denyAnonymousReadAccess>true</denyAnonymousReadAccess>
  	</authorizationStrategy>
	<securityRealm class="hudson.security.HudsonPrivateSecurityRealm">
		{{ with .SecurityRealm}}
		<disableSignup>{{.DisableSignup}}</disableSignup>
		<enableCaptcha>{{.EnableCaptcha}}</enableCaptcha>
		{{ end }}
	</securityRealm>
	<disableRememberMe>{{.DisableRememberMe}}</disableRememberMe>
	<workspaceDir>${ITEM_ROOTDIR}/workspace</workspaceDir>
	<buildsDir>${ITEM_ROOTDIR}/builds</buildsDir>
	<jdks/>
	<viewsTabBar class="hudson.views.DefaultViewsTabBar"/>
	<myViewsTabBar class="hudson.views.DefaultMyViewsTabBar"/>
	<clouds>	
	{{ with .CloudTemplates -}}
		{{ range . -}}
			{{.}}
		{{ end -}}
	{{- end}}
	</clouds>
	<quietPeriod></quietPeriod>
	<scmCheckoutRetryCount>{{.SCMCheckoutRetryCount}}</scmCheckoutRetryCount>
	<views>
		<hudson.model.AllView>
		<owner class="hudson" reference="../../.."/>
		<name>All</name>
		<filterExecutors>false</filterExecutors>
		<filterQueue>false</filterQueue>
		<properties/>
		</hudson.model.AllView>
	</views>
	<primaryView>All</primaryView>
	<slaveAgentPort>{{if gt .SlaveAgentPort 1}}{{.SlaveAgentPort}}{{else}}50000{{end}}</slaveAgentPort>
	<label>{{if ne .Label ""}}{{.Label}}{{else}}master{{end}}</label>
	<nodeProperties/>
	<globalNodeProperties/>
	<noUsageStatistics>{{.NoUsageStatitics}}</noUsageStatistics>
</hudson>
	`
)

// Jenkins jenkins
type Jenkins struct {
	Version       string `yaml:"version"`
	NumExecutors  uint   `yaml:"numExecutors"`
	Mode          string `yaml:"mode"`
	UseSecurity   bool   `yaml:"useSecurity"`
	SecurityRealm struct {
		DisableSignup bool `yaml:"disableSignup"`
		EnableCaptcha bool `yaml:"enableCaptcha"`
	} `yaml:"securityRealm"`
	DisableRememberMe     bool    `yaml:"disableRememberMe"`
	Clouds                []Cloud `yaml:"clouds"`
	CloudTemplates        []string
	SCMCheckoutRetryCount uint   `yaml:"scmCheckoutRetryCount"`
	SlaveAgentPort        uint   `yaml:"slaveAgentPort"`
	Label                 string `yaml:"label"`
	NoUsageStatitics      bool   `yaml:"noUsageStatistics"`
}

// Parse render a jenkins configuration
func (j Jenkins) Parse() (string, error) {
	return RenderJenkins(j)
}

// RenderJenkins render jenkins configuration file
func RenderJenkins(jen Jenkins) (content string, err error) {
	jen.CloudTemplates = make([]string, len(jen.Clouds))
	if len(jen.Clouds) > 0 {
		for i, c := range jen.Clouds {
			c.CloudTemplate, err = c.Parse()
			jen.CloudTemplates[i] = c.CloudTemplate
			if err != nil {
				return
			}
		}
	}
	// adding jenkins version from env var (added when building image)
	if viper.GetString("JENKINS_VERSION") != "" {
		jen.Version = viper.GetString("JENKINS_VERSION")
	}
	// add minimum of 2 executors
	if jen.NumExecutors == 0 {
		jen.NumExecutors = 2
	}
	byt := new(bytes.Buffer)
	if err = Render(jenkinsTemp, jen, byt); err != nil {
		return
	}
	content = byt.String()
	return
}

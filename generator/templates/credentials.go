package templates

import (
	"bytes"
)

const (
	credentialsTemp = `<?xml version='1.0' encoding='UTF-8'?>
<com.cloudbees.plugins.credentials.SystemCredentialsProvider plugin="credentials@2.1.13">
	<domainCredentialsMap class="hudson.util.CopyOnWriteMap$Hash">
	{{ range . -}}
			{{.}}
	{{- end }}
	</domainCredentialsMap>
</com.cloudbees.plugins.credentials.SystemCredentialsProvider>
`
	k8sCredentialsTemp = `
	<entry>
      <com.cloudbees.plugins.credentials.domains.Domain>
        <specifications/>
      </com.cloudbees.plugins.credentials.domains.Domain>
      <java.util.concurrent.CopyOnWriteArrayList>
        <org.csanchez.jenkins.plugins.kubernetes.ServiceAccountCredential plugin=\"kubernetes@0.4.1\">
          <scope>{{.Scope}}</scope>
          <id>{{.ID}}</id>
          <description>{{.Description}}</description>
        </org.csanchez.jenkins.plugins.kubernetes.ServiceAccountCredential>
      </java.util.concurrent.CopyOnWriteArrayList>
    </entry>
`
)

// CredentialSlice slice of credentials
type CredentialSlice []Credential

// Parse parses a template and returns the content
func (cred CredentialSlice) Parse() (content string, err error) {
	renders := make([]string, len(cred))
	
	for i, c := range cred {
		c.CredentialRender, err = c.Parse()
		if err != nil {
			return
		}
		renders[i] = c.CredentialRender
	}
	byt := new(bytes.Buffer)
	if err = Render(credentialsTemp, renders, byt); err != nil {
		return
	}
	content = byt.String()
	return
}

// Credential a credential structure
type Credential struct {
	Type string `yaml:"type"`
	Kubernetes KubernetesCredentias `yaml:"kubernetes"`
	CredentialRender string
}

// Parse will parse the respective configuration
func (c Credential) Parse() (content string, err error) {
	switch c.Type {
	case "kubernetes":
		return c.Kubernetes.Parse()
	}
	return
}

// KubernetesCredentias credentials for kubernetes
type KubernetesCredentias struct {
	Scope string `yaml:"scope"`
	ID string `yaml:"id"`
	Description string `yaml:"description"`
}

// Parse will render the configuration for kubernetes config
func (k KubernetesCredentias) Parse() (content string, err error) {
	byt := new(bytes.Buffer)
	if err = Render(k8sCredentialsTemp, k, byt); err != nil {
		return
	}
	content = byt.String()
	return
}

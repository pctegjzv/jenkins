package templates

import (
	"bytes"
	"fmt"

	"github.com/spf13/viper"
)

const (
	k8sTemp = `<org.csanchez.jenkins.plugins.kubernetes.KubernetesCloud>
		<name>{{.Name}}</name>
		<templates>
		{{ with .TemplatesRender -}}
			{{- range . -}}
				{{.}}
			{{- end -}}
		{{- end -}}
		</templates>
		<serverUrl>{{.ServerURL}}</serverUrl>
		<skipTlsVerify>{{.SkipTLSVerify}}</skipTlsVerify>
		<serverCertificate>{{.ServerCertificate}}</serverCertificate>
		<namespace>{{.Namespace}}</namespace>
		<jenkinsUrl>{{.JenkinsURL}}</jenkinsUrl>
		<jenkinsTunnel>{{.JNLPTunnelURL}}</jenkinsTunnel>
		<credentialsId>{{.CredentialsID}}</credentialsId>
		<containerCap>{{if .ContainerCap}}{{.ContainerCap}}{{else}}5{{end}}</containerCap>
		<retentionTimeout>{{.RetentionTimeout}}</retentionTimeout>
	</org.csanchez.jenkins.plugins.kubernetes.KubernetesCloud>`
	podTemp = `<org.csanchez.jenkins.plugins.kubernetes.PodTemplate>
		<inheritFrom>{{.InheritFrom}}</inheritFrom>
		<name>{{.Name}}</name>
		<label>{{.Label}}</label>
		<namespace>{{.Namespace}}</namespace>
		<privileged>{{.Privileged}}</privileged>
		<alwaysPullImage>{{.AlwaysPullImage}}</alwaysPullImage>
		<instanceCap>{{.InstanceCap}}</instanceCap>
		<slaveConnectTimeout>{{.ConnectTimeout}}</slaveConnectTimeout>
		<idleMinutes>{{.IdleMinutes}}</idleMinutes>
		<nodeSelector></nodeSelector>
		<nodeUsageMode>{{.NodeUsage}}</nodeUsageMode>
		<customWorkspaceVolumeEnabled>{{.CustomWorkspaceVolumeEnabled}}</customWorkspaceVolumeEnabled>
		{{ if .CustomWorkspaceVolumeEnabled -}}
			{{ with .WorkspaceVolume -}}
				{{ if eq .Type "hostPath" -}}
					<workspaceVolume class="org.csanchez.jenkins.plugins.kubernetes.volumes.workspace.HostPathWorkspaceVolume">
					{{ with .HostPath -}}
						<hostPath>{{.Path}}</hostPath>
					{{- end}}
					</workspaceVolume>
				{{- end -}}
				{{ if eq .Type "persistentVolumeClaim" -}}
					<workspaceVolume class="org.csanchez.jenkins.plugins.kubernetes.volumes.workspace.PersistentVolumeClaimWorkspaceVolume">
					{{ with .PersistentVolumeClaim -}}
						<claimName>{{.ClaimName}}</claimName>
						<readOnly>{{.ReadOnly}}</readOnly>
					{{- end }}
					</workspaceVolume>
				{{- end -}}
			{{- end -}}
		{{- end }}
		<volumes>
		{{ with .Volumes -}}
			{{ range . -}}
				{{ if eq .Type "hostPath" }}
			<org.csanchez.jenkins.plugins.kubernetes.volumes.HostPathVolume>
				{{ with .HostPath }}
				<mountPath>{{.MountPath}}</mountPath>
				<hostPath>{{.HostPath}}</hostPath>
				{{- end }}
			</org.csanchez.jenkins.plugins.kubernetes.volumes.HostPathVolume>
				{{end}}
				{{ if eq .Type "persistentVolumeClaim" }}
					<org.csanchez.jenkins.plugins.kubernetes.volumes.PersistentVolumeClaim>
						{{ with .PersistentVolumeClaim}}
						<mountPath>{{.MountPath}}</mountPath>
						<claimName>{{.ClaimName}}</claimName>
						<readOnly>{{.ReadOnly}}</readOnly>
						{{- end }}
					</org.csanchez.jenkins.plugins.kubernetes.volumes.PersistentVolumeClaim>
				{{- end }}
			{{- end }}
		{{- end }}
		</volumes>
		<containers>
		{{ with .Containers }}
			{{ range . }}
				<org.csanchez.jenkins.plugins.kubernetes.ContainerTemplate>
				<name>{{.Name}}</name>
				<image>{{.Image}}</image>
				<privileged>{{.Privileged}}</privileged>
				<alwaysPullImage>{{.AlwaysPullImage}}</alwaysPullImage>
				<workingDir>{{.WorkingDir}}</workingDir>
				<command>{{.Command}}</command>
				<args>{{.Args}}</args>
				<ttyEnabled>{{.TTY}}</ttyEnabled>
				<resourceRequestCpu>{{if .RequestCPU}}{{.RequestCPU}}{{end}}</resourceRequestCpu>
				<resourceRequestMemory>{{if .RequestMemory}}{{.RequestMemory}}{{end}}</resourceRequestMemory>
				<resourceLimitCpu>{{if .LimitCPU}}{{.LimitCPU}}{{end}}</resourceLimitCpu>
				<resourceLimitMemory>{{if .LimitMemory}}{{.LimitMemory}}{{end}}</resourceLimitMemory>
				<envVars/>
				<ports/>
				<livenessProbe>
					<execArgs></execArgs>
					<timeoutSeconds>0</timeoutSeconds>
					<initialDelaySeconds>0</initialDelaySeconds>
					<failureThreshold>0</failureThreshold>
					<periodSeconds>0</periodSeconds>
					<successThreshold>0</successThreshold>
				</livenessProbe>
				</org.csanchez.jenkins.plugins.kubernetes.ContainerTemplate>
			{{ end }}
		{{ end }}
		</containers>
		<envVars/>
		<annotations/>
		<imagePullSecrets>
		{{ with .ImagePullSecrets -}}
			{{- range . -}}
				<org.csanchez.jenkins.plugins.kubernetes.PodImagePullSecret>
				<name>{{- .Name -}}</name>
				</org.csanchez.jenkins.plugins.kubernetes.PodImagePullSecret>
			{{- end -}}
		{{- end }}
		</imagePullSecrets>
	</org.csanchez.jenkins.plugins.kubernetes.PodTemplate>`
)

// Cloud clouds configuration
type Cloud struct {
	Type       string     `yaml:"type"`
	Kubernetes Kubernetes `yaml:"kubernetes"`
	// each should parse its own template
	CloudTemplate string
}

// Parse parse and render template
func (c Cloud) Parse() (content string, err error) {
	switch c.Type {
	case "kubernetes":
		content, err = c.Kubernetes.Parse()
		c.CloudTemplate = content
	}
	return
}

// Kubernetes kubernetes configuration
type Kubernetes struct {
	Name              string     `yaml:"name"`
	ServerURL         string     `yaml:"serverUrl"`
	SkipTLSVerify     bool       `yaml:"skipTlsVerify"`
	ServerCertificate string     `yaml:"serverCertificate"`
	Namespace         string     `yaml:"namespace"`
	JenkinsURL        string     `yaml:"jenkinsUrl"`
	JNLPTunnelURL     string     `yaml:"jenkinsTunnel"`
	CredentialsID     string     `yaml:"credentialsId"`
	ContainerCap      uint       `yaml:"containerCap"`
	RetentionTimeout  uint       `yaml:"retentionTimeout"`
	Templates         []Template `yaml:"templates"`
	TemplatesRender   []string
}

// Parse parse and render
func (k Kubernetes) Parse() (temp string, err error) {
	// try to fetch information from the container for kubernetes information
	if IsKubernetesPod() {
		if k.ServerCertificate == "" {
			k.ServerCertificate = GetKubernetesCA()
		}
		if k.Namespace == "" {
			k.Namespace = GetKubernetesNamespace()
		}
		// if not found will fetch from env vars
		if k.Namespace == "" {
			k.Namespace = viper.GetString("KUBERNETES_NAMESPACE")
		}

		// check the kubernetes configuration in cluster
		if k.ServerURL == "" && viper.GetString("KUBERNETES_SERVICE_HOST") != "" && viper.GetString("KUBERNETES_PORT_443_TCP_PORT") != "" {
			k.ServerURL = fmt.Sprintf(
				"https://%s:%s",
				viper.GetString("KUBERNETES_SERVICE_HOST"),
				viper.GetString("KUBERNETES_PORT_443_TCP_PORT"),
			)
		}

		// getting kubernetes address from env vars
		if k.ServerURL == "" {
			k.ServerURL = viper.GetString("KUBERNETES_URL")
		}
		// if still not there

		// jenkins service information
		if k.JenkinsURL == "" {
			// try to fetch from kubernetes added environment variables
			// will look for it as a service name
			serviceName := viper.GetString("JENKINS_SERVICE_NAME")
			jenkinsHostEnvVar := fmt.Sprintf("%s_SERVICE_HOST", serviceName)
			jenkinsPortEnvVar := fmt.Sprintf("%s_SERVICE_PORT", serviceName)
			if viper.GetString(jenkinsHostEnvVar) != "" {
				k.JenkinsURL = fmt.Sprintf(
					"http://%s:%s",
					viper.GetString(jenkinsHostEnvVar),
					viper.GetString(jenkinsPortEnvVar),
				)
			}
		}

		// trying to find the JNLP port for jenkins
		if k.JNLPTunnelURL == "" {
			var (
				jenkinsHostEnvVar string
				jenkinsPortEnvVar string
			)
			if viper.GetString("JNLP_SERVICE_NAME") != viper.GetString("JENKINS_SERVICE_NAME") {
				// if there is a specific service
				// this is used when both ports are split
				// into two different services
				serviceName := viper.GetString("JNLP_SERVICE_NAME")
				jenkinsHostEnvVar = fmt.Sprintf("%s_SERVICE_HOST", serviceName)
				jenkinsPortEnvVar = fmt.Sprintf("%s_SERVICE_PORT", serviceName)
			} else {
				// if is the same service we can use another port
				// generally it will be signed as JNLP
				serviceName := viper.GetString("JNLP_SERVICE_NAME")
				jenkinsHostEnvVar = fmt.Sprintf("%s_SERVICE_HOST", serviceName)
				jenkinsPortEnvVar = fmt.Sprintf("%s_SERVICE_PORT_JNLP", serviceName)
			}
			if viper.GetString(jenkinsHostEnvVar) != "" && viper.GetString(jenkinsPortEnvVar) != "" {
				k.JNLPTunnelURL = fmt.Sprintf(
					"%s:%s",
					viper.GetString(jenkinsHostEnvVar),
					viper.GetString(jenkinsPortEnvVar),
				)
			}
		}

		// if not found yet we will try to use the jenkins service with
		// a fixed port number
		if k.JNLPTunnelURL == "" {
			// if there is a specific service
			// this is used when both ports are split
			// into two different services
			serviceName := viper.GetString("JENKINS_SERVICE_NAME")
			jenkinsHostEnvVar := fmt.Sprintf("%s_SERVICE_HOST", serviceName)
			if viper.GetString(jenkinsHostEnvVar) != "" {
				k.JNLPTunnelURL = fmt.Sprintf(
					"%s:50000",
					viper.GetString(jenkinsHostEnvVar),
				)
			}
		}

		// defaults to 30 containers in cluster
		if k.ContainerCap <= 0 {
			k.ContainerCap = 30
		}
	}
	k.TemplatesRender = make([]string, len(k.Templates))
	if len(k.Templates) > 0 {

		for i, t := range k.Templates {
			t.TemplateRender, err = t.Parse()
			k.TemplatesRender[i] = t.TemplateRender
			if err != nil {
				return
			}
		}
	}
	buf := new(bytes.Buffer)
	err = Render(k8sTemp, k, buf)
	if err == nil {
		temp = buf.String()
	}
	return
}

// Template templats for k8s
type Template struct {
	Type           string `yaml:"type"`
	Pod            Pod    `yaml:"pod"`
	TemplateRender string
}

// Parse parse and render
func (t Template) Parse() (content string, err error) {
	switch t.Type {
	case "pod":
		content, err = t.Pod.Parse()
	}
	return
}

// Pod structure
type Pod struct {
	InheritFrom                  string `yaml:"inheritFrom"`
	Name                         string `yaml:"name"`
	Label                        string `yaml:"label"`
	Namespace                    string `yaml:"namespace"`
	Privileged                   bool   `yaml:"privileged"`
	AlwaysPullImage              bool   `yaml:"alwaysPullImage"`
	InstanceCap                  uint   `yaml:"instanceCap"`
	ConnectTimeout               uint   `yaml:"connectTimeout"`
	IdleMinutes                  uint   `yaml:"idleMinutes"`
	ServiceAccount               string `yaml:"serviceAccount"`
	NodeUsage                    string `yaml:"nodeUsage"`
	CustomWorkspaceVolumeEnabled bool   `yaml:"customWorkspaceVolumeEnabled"`
	WorkspaceVolume              struct {
		Type     string `yaml:"type"`
		HostPath struct {
			Path string `yaml:"path"`
		} `yaml:"hostPath"`
		PersistentVolumeClaim struct {
			ClaimName string `yaml:"claimName"`
			ReadOnly  bool   `yaml:"readOnly"`
		} `yaml:"persistentVolumeClaim"`
	} `yaml:"workspaceVolume,omitempty"`
	Volumes          []PodVolumes `yaml:"volumes"`
	Containers       []Container  `yaml:"containers"`
	ImagePullSecrets []Secret     `yaml:"imagePullSecrets"`
}

// Parse parse and render
func (pod Pod) Parse() (content string, err error) {

	if pod.InstanceCap <= 0 {
		pod.InstanceCap = 2147483647
	}

	var byt = new(bytes.Buffer)
	err = Render(podTemp, pod, byt)
	if err == nil {
		content = byt.String()
	}
	return
}

// PodVolumes volumes for pods
type PodVolumes struct {
	Type     string `yaml:"type"`
	HostPath struct {
		MountPath string `yaml:"mountPath"`
		HostPath  string `yaml:"hostPath"`
	} `yaml:"hostPath"`
	PersistentVolumeClaim struct {
		MountPath string `yaml:"mountPath"`
		ClaimName string `yaml:"claimName"`
		ReadOnly  bool   `yaml:"readOnly"`
	} `yaml:"persistentVolumeClaim"`
}

// Container container struct
type Container struct {
	Name            string `yaml:"name"`
	Image           string `yaml:"image"`
	Privileged      bool   `yaml:"privileged"`
	AlwaysPullImage bool   `yaml:"alwaysPullImage"`
	WorkingDir      string `yaml:"workingDir"`
	Command         string `yaml:"command"`
	Args            string `yaml:"args"`
	TTY             bool   `yaml:"ttyEnabled"`
	RequestCPU      uint   `yaml:"resourceRequestCpu"`
	RequestMemory   uint   `yaml:"resourceRequestMemory"`
	LimitCPU        uint   `yaml:"resourceLimitCpu"`
	LimitMemory     uint   `yaml:"resourceLimitMemory"`
}

// Secret secret struct
type Secret struct {
	Name string `yaml:"name"`
}

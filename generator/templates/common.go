package templates

import (
	"text/template"
	"io"
	"io/ioutil"
	"os"
	"gopkg.in/yaml.v2"
	"errors"
)

// Config general configuration file for all structs
type Config struct {
	Jenkins Jenkins `yaml:"jenkins"`
	Alauda AlaudaPipeline `yaml:"alauda"`
	Credentials CredentialSlice `yaml:"credentials"`
}

// LoadConfig will load the whole configuration at once
func LoadConfig(filePath string) (config Config, err error) {
	var content []byte
	if content, err = LoadFromFile(filePath); err != nil {
		return
	}
	err = yaml.Unmarshal(content, &config)
	return
}

// LoadFromFile loads the content of a file and returns as a []byte]
func LoadFromFile(filePath string) (content []byte, err error) {
	var (
		file *os.File
	)
	if file, err = os.Open(filePath); err != nil {
		return
	}
	defer file.Close()
	content, err = ioutil.ReadAll(file);
	return
}

// LoadFromFileString load file contents and return as string
func LoadFromFileString(filePath string) (content string, err error) {
	var byt []byte
	byt, err = LoadFromFile(filePath)
	if err == nil {
		content = string(byt)
	}
	return
}

// Render initiates and renders a template to a writter
func Render(tempContent string, data interface{}, writter io.Writer, funcs ...template.FuncMap) (err error) {
	var t *template.Template
	if t, err = template.New("template").Parse(tempContent); err != nil {
		return 
	}
	for _, d := range funcs {
		t = t.Funcs(d)
	}
	err = t.Execute(writter, data)
	return
}

// DoesFileExist returns true if a file already exists
func DoesFileExist(filePath string) (exist bool) {
	exist = true
	if _, osErr := os.Stat(filePath); os.IsNotExist(osErr) {
		exist = false
	}
	return
}

// ReadGenericYAML read a given file as yaml and return its content as interface{}
func ReadGenericYAML(file string) (content interface{}, err error) {
	if !DoesFileExist(file) {
		err = ErrNotFileExist
		return
	}
	var by []byte
	by, err = LoadFromFile(file)
	if err != nil {
		return
	}
	err = yaml.Unmarshal(by, &content)
	
	return
}

var (
	ErrFileExist = errors.New("File already exists")
	ErrNotFileExist = errors.New("File does not exist")
)

const (
	serviceAccountDir = "/run/secrets/kubernetes.io/serviceaccount" 
	namespaceFile = serviceAccountDir+"/namespace"
	caFile = serviceAccountDir+"/ca.crt"
	tokenFile = serviceAccountDir+"/token"
)

// IsKubernetesPod returns true if the container was deployed in a kubernetes pod
func IsKubernetesPod() bool {
	return DoesFileExist(serviceAccountDir)
}

// GetKubernetesToken reads the token from service account folder
func GetKubernetesToken() (token string) {
	token = readFile(tokenFile)
	return
}

// GetKubernetesCA reads kubernetes CA file
func GetKubernetesCA() (ca string) {
	ca = readFile(caFile)
	return
}

// GetKubernetesNamespace fetches kubernetes namespace
func GetKubernetesNamespace() (namespace string) {
	namespace = readFile(namespaceFile)
	return
}

func readFile(filePath string) (content string) {
	if DoesFileExist(filePath) {
		byt, _ := ioutil.ReadFile(filePath)
		content = string(byt)
	}
	return
}
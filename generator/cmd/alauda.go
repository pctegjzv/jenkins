// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"devops/jenkins/generator/templates"

)

// alaudaCmd represents the alauda command
var alaudaCmd = &cobra.Command{
	Use:   "alauda",
	Short: "generates configuration for alauda plugin",
	Run: func(cmd *cobra.Command, args []string) {
		// verifies if destination file exists
		// if it exists and it is not force should fail
		if destinFile != "" && DoesFileExist(destinFile) && !force {
			fmt.Println("file already exists:", destinFile)
			os.Exit(2)
		}

		// parse yaml
		config := templates.Config{}
		err := viper.Unmarshal(&config)
		if err !=nil{
			fmt.Println("loading configuration err", err)
			os.Exit(2)
		}

		conte, err := config.Alauda.Parse()
		if destinFile != "" {
			err = WriteToFile(conte, destinFile, force)
			if err != nil {
				fmt.Println("error writing file", err)
				os.Exit(2)
			}
		} else {
			fmt.Println(conte)
		}
	},
}

func init() {
	rootCmd.AddCommand(alaudaCmd)
}

// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"devops/jenkins/generator/templates"
)

// credentialsCmd represents the credentials command
var credentialsCmd = &cobra.Command{
	Use:   "credentials",
	Short: "generates jenkins configuration for credentials",
	Run: func(cmd *cobra.Command, args []string) {
		// verifies if destination file exists
		// if it exists and it is not force should fail
		if destinFile != "" && DoesFileExist(destinFile) && !force {
			fmt.Println("file already exists:", destinFile)
			os.Exit(2)
		}
		// parse yaml
		config, err := templates.LoadConfig(viper.ConfigFileUsed())
		if err != nil {
			fmt.Println("loading configuration err", err)
			os.Exit(2)
		}
		conte, err := config.Credentials.Parse()
		if destinFile != "" {
			err = WriteToFile(conte, destinFile, force)
			if err != nil {
				fmt.Println("error writing file", err)
				os.Exit(2)
			}
		} else {
			fmt.Println(conte)
		}
	},
}

func init() {
	rootCmd.AddCommand(credentialsCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// credentialsCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// credentialsCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"strings"
	"fmt"
	"os"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	cfgFile string
	destinFile string
	force bool
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "generator",
	Short: "creates jenkins configuration files based on a yaml",
	// Uncomment the following line if your bare application
	// has an action associated with it:
	//	Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// shared flags among all commands
	rootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "", "config file (default is $HOME/jenkins.yaml)")
	rootCmd.PersistentFlags().StringVarP(&destinFile, "file", "f", "", "destination file (defaults to print on stdout)")
	rootCmd.PersistentFlags().BoolVarP(&force, "force", "", false, "force write to file")

}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".generator" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName("jenkins")
	}

	// Setting defaults

	// Kubernetes
	viper.SetDefault("KUBERNETES_NAMESPACE", "default")
	viper.SetDefault("KUBERNETES_URL", "https://kubernetes.default.svc.cluster.local")
	viper.SetDefault("JENKINS_SERVICE_NAME", "JENKINS")
	viper.SetDefault("DEFAULT_SLAVE_DIRECTORY", "/tmp")
	viper.SetDefault("POD_VOLUMES_HOSTPATH","/data")
	viper.SetDefault("PERSISTENT_VOLUMECLAIM","/data")

	viper.AutomaticEnv() // read in environment variables that match
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil && destinFile != "" {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}

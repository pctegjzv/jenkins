// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/spf13/viper"

	"devops/jenkins/generator/templates"

	"github.com/spf13/cobra"
)

var (
	k8sEnabled bool
)

// jenkinsCmd represents the jenkins command
var jenkinsCmd = &cobra.Command{
	Use:   "jenkins",
	Short: "Generate main jenkins configuration",
	Run: func(cmd *cobra.Command, args []string) {
		// verifies if destination file exists
		// if it exists and it is not force should fail
		if destinFile != "" && DoesFileExist(destinFile) && !force {
			fmt.Println("file already exists:", destinFile)
			os.Exit(2)
		}

		config := InitConfig()

		err := viper.Unmarshal(&config)
		if err != nil {
			fmt.Println("loading configuration err", err)
			os.Exit(2)
		}

		conte, err := config.Jenkins.Parse()
		if err != nil {
			fmt.Printf("error parse jenkins: %s. \n", err)
			os.Exit(2)
		}
		if destinFile != "" {
			err = WriteToFile(conte, destinFile, force)
			if err != nil {
				fmt.Println("error writing file", err)
				os.Exit(2)
			}
		} else {
			fmt.Println(conte)
		}
	},
}

func init() {
	rootCmd.AddCommand(jenkinsCmd)
}

func InitConfig() templates.Config {
	config := templates.Config{}

	initJenkins(&config)

	podTemplates := initPodTemplate()
	if len(podTemplates) > 0 {
		k8s := initK8S(podTemplates)
		cloud := initCloud(k8s)
		config.Jenkins.Clouds = append(config.Jenkins.Clouds, *cloud)
	}
	return config

}

func initJenkins(config *templates.Config) {
	if viper.GetString("JENKINS_VERSION") != "" {
		config.Jenkins.Version = viper.GetString("JENKINS_VERSION")
	}
	config.Jenkins.Mode = "NORMAL"
	config.Jenkins.UseSecurity = true
	config.Jenkins.SecurityRealm.DisableSignup = true
}

func initPodTemplate() []templates.Template {
	var mountDirorName string
	var mountType string
	var slavePods = make(map[string]string)

	slaves := viper.GetString("SLAVES")
	var slaveslice = strings.Split(slaves, ",")
	for _, slavename := range slaveslice {
		indexslavename := "SLAVE_" + slavename
		if viper.GetString(indexslavename) != "" {
			slavePods[slavename] = viper.GetString(indexslavename)
		}
	}
	if viper.GetString("HOSTPATH") != "" {
		mountType = "hostPath"
		mountDirorName = viper.GetString("HOSTPATH")
	}

	if viper.GetString("PERSISTENTVOLUMECLAIM") != "" {
		mountType = "persistentVolumeClaim"
		mountDirorName = viper.GetString("PERSISTENTVOLUMECLAIM")
	}

	podTemplates := new(templates.Kubernetes).Templates
	for key, value := range slavePods {
		container := new(templates.Container)
		container.Image = value
		container.Name = "jnlp"
		container.AlwaysPullImage = true
		container.WorkingDir = "/home/Jenkins"
		container.Args = "${computer.jnlpmac} ${computer.name}"

		podvolumes := new(templates.PodVolumes)
		podvolumes.Type = mountType
		if podvolumes.Type == "hostPath" {
			podvolumes.HostPath.MountPath = mountDirorName
			podvolumes.HostPath.HostPath = viper.GetString("POD_VOLUMES_HOSTPATH")

		} else if podvolumes.Type == "persistentVolumeClaim" {
			podvolumes.PersistentVolumeClaim.ClaimName = mountDirorName
			podvolumes.PersistentVolumeClaim.MountPath = viper.GetString("PERSISTENT_VOLUMECLAIM")
		} else {
			log.Printf("The type you entered is unknown, no data volume for pod %s will be added", strings.ToLower(key))
		}

		pod := new(templates.Pod)
		pod.Containers = append(pod.Containers, *container)
		pod.Volumes = append(pod.Volumes, *podvolumes)
		pod.Name = strings.ToLower(key)
		pod.Label = pod.Name
		if viper.GetString("K8S_SLAVE_NAMESPACE") != "" {
			pod.Namespace = viper.GetString("K8S_SLAVE_NAMESPACE")
		} else {
			pod.Namespace = "default"
		}
		pod.InstanceCap = 500
		pod.ConnectTimeout = 100
		//Just a temporary plan, we will use Agent Manager to slove this problem in the near future
		pod.NodeUsage = "EXCLUSIVE"
		template := new(templates.Template)
		template.Type = "pod"
		template.Pod = *pod

		podTemplates = append(podTemplates, *template)
	}
	return podTemplates
}

func initK8S(podTemplates []templates.Template) templates.Kubernetes {
	k8s := new(templates.Kubernetes)
	k8s.Name = "kubernetes"
	k8s.SkipTLSVerify = true
	// k8s.CredentialsID = "1a12dfa4-7fc5-47a7-aa17-cc56572a41c7"
	k8s.ServerCertificate = viper.GetString("SERVER_CERTIFICATE")
	k8s.ContainerCap = 40
	k8s.RetentionTimeout = 5
	k8s.Templates = podTemplates

	return *k8s
}

func initCloud(k8s templates.Kubernetes) *templates.Cloud {
	cloud := new(templates.Cloud)
	cloud.Type = "kubernetes"
	cloud.Kubernetes = k8s
	return cloud
}

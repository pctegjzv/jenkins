package cmd

import (
	"io/ioutil"

	"devops/jenkins/generator/templates"
)

// DoesFileExist returns true if a file already exists
func DoesFileExist(filePath string) (bool) {
	return templates.DoesFileExist(filePath)
}

// WriteToFile writes a content to a file
func WriteToFile(content, file string, force bool) (err error) {
	if DoesFileExist(file) && !force {
		err = templates.ErrFileExist
		return
	}
	err = ioutil.WriteFile(file, []byte(content), 0770)
	return 
}

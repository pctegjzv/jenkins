# Jenkins images for Alauda platform

[TOC]

## Structure
 - Base Image          jenkins:base
 - Jenkins Master      jenkins:master
   - Jenkins Alauda    alauda-jenkins
   - Jenkins Devops    devops-jenkins
 - Slave
   - Base Slave       jenkins:node
   - Java             jenkins-node-java
   - C/C++        
   - Go               jenkins-node-golang
   - Python           jenkins-node-python

## Building
Use the Jenkinsfile available to build all the images

The build process should follow this sequence:


## Base Image

 - Basic building tools (GCC etc)
 - Docker
 - Kubectl
 - git
 - svn


## Jenkins Master

## Jenkins Alauda

## Jenkins Devops (TBD)

## Slave

## Note
jenkins-node-sonar will download **sonar-scanner** with jre. If jre version does not equal to java version which in jenkins image, scanner will encounter error because of not equal java runtime version. So sonar-scanner version is stick to 3.2.0.1227.
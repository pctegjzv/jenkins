#!/bin/bash
# this file will generate a credentials file for the admin user
# if the file already exists it will just quit

# verifying already existing configuration
if [ -f "$JENKINS_HOME/generated" ]; then
    echo "admin user already exists."
    exit 1;
fi

# main function
function gen_password() {
    local password="$1"
    local salt="$2"
    local acegi_security_path=`find /tmp/war/WEB-INF/lib/ -name acegi-security-*.jar`
    local commons_codec_path=`find /tmp/war/WEB-INF/lib/ -name commons-codec-*.jar`
    # source for password-encoder.jar is inside the jar.
    # acegi-security-1.0.7.jar is inside the jenkins war.
    java -classpath "${acegi_security_path}:${commons_codec_path}:$UTILS_DIR/password-encoder.jar" com.redhat.openshift.PasswordEncoder $password $salt
}

function save_admin_password() {
    sed -i "s,<passwordHash>.*</passwordHash>,<passwordHash>$new_password_hash</passwordHash>,g" "${JENKINS_HOME}/users/admin/config.xml"
    echo $new_password_hash > ${JENKINS_HOME}/password
}

# extract jenkins jar files
mkdir -p /tmp/war
unzip -q $JENKINS_INSTALL_DIR/jenkins.war -d /tmp/war
if [ -e ${JENKINS_HOME}/password ]; then
    old_salt=$(cat ${JENKINS_HOME}/password | sed 's/:.*//')
fi

# generate password hash
new_password_hash=`gen_password ${JENKINS_PASSWORD:-password} $old_salt`

# saving the admin password to the file
save_admin_password

# setting as already generated file
touch $JENKINS_HOME/generated

# removing extracted files
rm -rf /tmp/war
#!/bin/bash
###################
# Alauda Jenkins 
###################
# Intro: this is a jenkins bootstrap script
# 
# Steps:
# - If not configured this script will start bootstraping configuration
# - A yaml configuration file can be used to setup this jenkins (check docs)
# - If a JENKINS_CONFIGURATION env var is given it will use as a jenkins.yaml file
# - If not provided it will use $HOME/jenkins.yaml by default
# - it will generate the following configuration:
# -- jenkins general configuration (includes kubernetes integration)
# -- credentials for kubernetes integration
# -- alauda plugin integration (if $USE_ALAUDA_CONFIGURATION is provided)
# -- generate admin password (use $JENKINS_PASSWORD)
# -- generate any extra configuration (used to extend this image
# -- install any extra plugins (if $PLUGINS_FILE is provided)
# - will detect recommended java parameters based on the container limitations
sync
chmod +x /usr/local/bin/*
sync
# basic bootstrap script for Jenkins
if [ ! -e ${JENKINS_HOME}/configured ]; then
    # not configured yet

    # users can set this configuration file (should be yaml)
    if [ -z ${JENKINS_CONFIGURATION} ]; then
        echo "WARNING: JENKINS_CONFIGURATION is not set ,will use ${HOME}/jenkins.yaml"
        JENKINS_CONFIGURATION=${HOME}/jenkins.yaml
    elif [ ! -e ${JENKINS_CONFIGURATION} ];then
        echo "ERROR: Jenkins configuration file: ${JENKINS_CONFIGURATION} does not exist!"
        exit 1
    fi

    # copying default basic configuration from config folder
    if [ -d ${JENKINS_CONFIG_DIR} ]; then
        echo "copying configuration..."
        mv ${JENKINS_CONFIG_DIR}/* $JENKINS_HOME
    fi

    # generating basic jenkins configuration

    /usr/local/bin/generator jenkins -c $JENKINS_CONFIGURATION -f $JENKINS_HOME/config.xml

    # credentials configuration
    /usr/local/bin/generator credentials -c $JENKINS_CONFIGURATION -f $JENKINS_HOME/credentials.xml
    if [ -n ${USE_ALAUDA_CONFIGURATION} ]; then
        # alauda pipeline configuration
        /usr/local/bin/generator alauda -c $JENKINS_CONFIGURATION -f $JENKINS_HOME/io.alauda.jenkins.plugins.pipeline.AlaudaConfiguration.xml
    fi
    
    # creating admin user
    source /usr/local/bin/gen-admin-password.sh

    # invoking users's script
    source /usr/local/bin/extra-configuration.sh

    # install any extra plugins if needed
    if [ -f "${PLUGINS_FILE}" ]; then
        /usr/local/bin/install-plugins.sh ${PLUGINS_FILE}
    fi

    # move plugins to JENKINS_HOME
    if [ -d "${JENKINS_PLUGINS_DIR}" ]; then
        echo "copying plugins..."
        mkdir -p ${JENKINS_HOME}/plugins
        mv ${JENKINS_PLUGINS_DIR}/* ${JENKINS_HOME}/plugins
    fi

    /usr/local/bin/fix-permissions ${JENKINS_HOME}/plugins

    touch ${JENKINS_HOME}/configured
fi

# creating logging properties
source /usr/local/bin/gen-logging-properties.sh

# preparing to start jenkins
# load java run time options
source /usr/local/bin/java-opts.sh
# if JENKINS_JAVA_OPTIONS not set will use recommended java params
if [[ -z "${JENKINS_JAVA_OPTIONS}" ]]; then
  JENKINS_JAVA_OPTIONS="$JAVA_GC_OPTS $JAVA_INITIAL_HEAP_PARAM $JAVA_MAX_HEAP_PARAM $JAVA_CORE_LIMIT $JAVA_DIAGNOSTICS -Dfile.encoding=UTF8"
fi
# if `docker run` first argument start with `--` the user is passing jenkins launcher arguments
if [[ $# -lt 1 ]] || [[ "$1" == "--"* ]]; then
  set -x
  exec java $JENKINS_JAVA_OPTIONS $JENKINS_JAVA_EXTRA_OPTIONS  -Duser.home=${HOME} \
            -Djava.util.logging.config.file=/var/jenkins_home/logging.properties \
            -Djavamelody.application-name=${JENKINS_SERVICE_NAME} \
            -jar $JENKINS_INSTALL_DIR/jenkins.war $JENKINS_OPTS "$@"
fi

# As argument is not jenkins, assume user want to run his own process, for sample a `bash` shell to explore this image
exec "$@"
#!/bin/bash

# will generate a number of parameters for running java containers
# this will be useful to set the parameters according to the container
# size (memory, cpus)
# will generate the following parameters
# $JAVA_GC_OPTS $JAVA_INITIAL_HEAP_PARAM $JAVA_MAX_HEAP_PARAM $JAVA_CORE_LIMIT $JAVA_DIAGNOSTICS
# will also export some container related data
# CONTAINER_HEAP_PERCENT = percentage to be used in heap
# CONTAINER_MEMORY_IN_BYTES
# CONTAINER_MEMORY_IN_MB

#NOTE:  periodically check https://ce-gitlab.usersys.redhat.com/ce/jboss-dockerfiles/blob/develop/scripts/os-java-run/added/java-default-options for updates
export CONTAINER_MEMORY_IN_BYTES=$(cat /sys/fs/cgroup/memory/memory.limit_in_bytes)
export CONTAINER_MEMORY_IN_MB=$((CONTAINER_MEMORY_IN_BYTES/2**20))

if [[ -z "${JAVA_TOOL_OPTIONS}" ]]; then
  # these options will automatically be picked up by any JVM process but can
  # be overridden on that process' command line.
  JAVA_TOOL_OPTIONS="-XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -Dsun.zip.disableMemoryMapping=true"
  export JAVA_TOOL_OPTIONS
fi

# assume k8s/docker memory limit was set if memory.limit_in_bytes < 1TiB
if [[ "${CONTAINER_MEMORY_IN_BYTES}" -lt $((2**40)) ]]; then
  # set this JVM's -Xmx and -Xms if not set already (not propagated to any
  # child JVMs).  -Xmx can be calculated as a percentage, capped to a maximum,
  # or specified straight.  -Xms can be calculated as a percentage or
  # specified straight.  For the JNLP slave by default we specify -Xmx of 50%,
  # uncapped; -Xms unspecified (JVM default is 1/64 of -Xmx).

  if [[ -z "$CONTAINER_HEAP_PERCENT" ]]; then
      export CONTAINER_HEAP_PERCENT=0.50
  fi

  CONTAINER_HEAP_MAX=$(echo "${CONTAINER_MEMORY_IN_MB} ${CONTAINER_HEAP_PERCENT}" | awk '{ printf "%d", $1 * $2 }')
  if [[ $JENKINS_MAX_HEAP_UPPER_BOUND_MB && $CONTAINER_HEAP_MAX -gt $JENKINS_MAX_HEAP_UPPER_BOUND_MB ]]; then
    export CONTAINER_HEAP_MAX=$JENKINS_MAX_HEAP_UPPER_BOUND_MB
  fi
  if [[ -z "$JAVA_MAX_HEAP_PARAM" ]]; then
    export JAVA_MAX_HEAP_PARAM="-Xmx${CONTAINER_HEAP_MAX}m"
  fi

  if [[ "$CONTAINER_INITIAL_PERCENT" ]]; then
    CONTAINER_INITIAL_HEAP=$(echo "${CONTAINER_HEAP_MAX} ${CONTAINER_INITIAL_PERCENT}" | awk '{ printf "%d", $1 * $2 }')
    if [[ -z "$JAVA_INITIAL_HEAP_PARAM" ]]; then
      export JAVA_INITIAL_HEAP_PARAM="-Xms${CONTAINER_INITIAL_HEAP}m"
    fi
  fi
fi

if [[ -z "$JAVA_GC_OPTS" ]]; then
  # See https://developers.redhat.com/blog/2014/07/22/dude-wheres-my-paas-memory-tuning-javas-footprint-in-openshift-part-2/ .
  # The values are aggressively set with the intention of relaxing GC CPU time
  # restrictions to enable it to free as much as possible, as well as
  # encouraging the GC to free unused heap memory back to the OS.
  export JAVA_GC_OPTS="-XX:+UseParallelGC -XX:MinHeapFreeRatio=5 -XX:MaxHeapFreeRatio=10 -XX:GCTimeRatio=4 -XX:AdaptiveSizePolicyWeight=90"
fi

if [[ "${USE_JAVA_DIAGNOSTICS}" || "${JAVA_DIAGNOSTICS}" ]]; then
  echo "Warning: USE_JAVA_DIAGNOSTICS and JAVA_DIAGNOSTICS are legacy and may be removed in a future version of this script."
fi

if [[ "${USE_JAVA_DIAGNOSTICS}" ]]; then
  export JAVA_DIAGNOSTICS="-XX:NativeMemoryTracking=summary -XX:+PrintGC -XX:+PrintGCDateStamps -XX:+PrintGCTimeStamps -XX:+UnlockDiagnosticVMOptions"
fi

if [[ "${CONTAINER_CORE_LIMIT}" ]]; then
  export JAVA_CORE_LIMIT="-XX:ParallelGCThreads=${CONTAINER_CORE_LIMIT} -Djava.util.concurrent.ForkJoinPool.common.parallelism=${CONTAINER_CORE_LIMIT} -XX:CICompilerCount=2"
fi
#!/bin/bash
# this file will generate logging properties file
# if the file already exists it will just quit

# verifying already existing configuration
LOGFILE=logging.properties

if [ -f "$JENKINS_HOME/${LOGFILE}" ]; then
    echo "${LOGFILE} already exists, will override it"
fi

LOG_LEVEL="${LOG_LEVEL}"
if [ -z "${LOG_LEVEL}" ]; then
  LOG_LEVEL="INFO"
fi

# main function
function gen_logging_properties() {
    cat << EOF >$JENKINS_HOME/${LOGFILE}
handlers=java.util.logging.FileHandler,java.util.logging.ConsoleHandler,
java.util.logging.ConsoleHandler.level=${LOG_LEVEL}
java.util.logging.ConsoleHandler.formatter=java.util.logging.SimpleFormatter

java.util.logging.FileHandler.level=${LOG_LEVEL}
java.util.logging.FileHandler.formatter=java.util.logging.SimpleFormatter
java.util.logging.FileHandler.limit=1024000
java.util.logging.FileHandler.count=5
java.util.logging.FileHandler.pattern=/var/log/jenkins/jenkins.log
java.util.logging.FileHandler.append=true
EOF
}

mkdir -p /var/log/jenkins

gen_logging_properties
## Environment Variables



| Env var                    | default                                  | description                              | optional |
| -------------------------- | ---------------------------------------- | ---------------------------------------- | -------- |
| JENKINS_URL                |                                          | url for accessing Jenkins                | N        |
| JENKINS_SERVICE_HOST       |                                          | jenkins host name (see bellow)           | Y        |
| JENKINS_SERVICE_PORT       |                                          | jenkins port <br />only used when container started with params<br />and `JENKINS_URL` is not provided | Y        |
| JENKINS_TUNNEL             |                                          | if `—tunnel` is provided it will use this variable to give a secondary address | Y        |
| JENKINS_SLAVE_SERVICE_HOST |                                          | if `--tunnel` is used and `JENKINS_TUNNEL` is not used will use both these variables |          |
| JENKINS_SLAVE_SERVICE_PORT |                                          | if `--tunnel` is used and `JENKINS_TUNNEL` is not used will use both these variables |          |
| JAVA_TOOL_OPTIONS          | ` "-XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -Dsun.zip.disableMemoryMapping=true"` | basic java arguments<br />sets if not available | Y        |
| CONTAINER_HEAP_PERCENT     | `0.5`                                    | percentage of heap                       | Y        |
| JAVA_MAX_HEAP_PARAM        | half of available memory in container    | java max heap parameter                  |          |
| JAVA_GC_OPTS               | ` -XX:+UseParallelGC -XX:MinHeapFreeRatio=5 -XX:MaxHeapFreeRatio=10 -XX:GCTimeRatio=4 -XX:AdaptiveSizePolicyWeight=90` | GC options for java                      |          |
| CONTAINER_CORE_LIMIT       |                                          | sets number of cores to java             |          |
| JNLP_JAVA_OPTIONS          | all the options above                    |                                          |          |
| JNLP_JAVA_OVERRIDES        |                                          | any custom overrides for JNLP java       |          |
| USE_JAVA_DIAGNOSTICS       |                                          | sets diagnostic flags to java            |          |


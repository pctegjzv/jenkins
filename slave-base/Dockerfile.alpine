# using the headless version of the jdk for slaves
FROM openjdk:8-jdk-alpine3.7
ENV HOME /home/jenkins
USER root
ARG VERSION=3.16
ARG AGENT_WORKDIR=/home/jenkins/agent

# 1. installs common software for slaves
# 2. purge caches
# 3. adjust folder structures
RUN apk update && \
    INSTALL_PKGS="alpine-sdk bash curl git subversion rsync tar unzip zip ca-certificates docker upx" && \
    apk add --no-cache $INSTALL_PKGS && \
    mkdir -p /home/jenkins && \
    chown -R 1001:0 /home/jenkins && \
    chmod -R g+w /home/jenkins && \
    chmod 775 /etc/passwd && \
    chmod 775 /usr/bin

# downloads and installs kubectl
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl \
    && chmod +x ./kubectl && mv kubectl /usr/local/bin && mkdir -p $HOME/.kube

# copy the entrypoint
ADD bin/* /usr/local/bin/

#####
####

# JAVA PART
ENV MAVEN_VERSION=3.3 \
    GRADLE_VERSION=4.2.1 \
    BASH_ENV=/usr/local/bin/scl_enable \
    ENV=/usr/local/bin/scl_enable \
    PROMPT_COMMAND=". /usr/local/bin/scl_enable" \
    PATH=$PATH:/opt/gradle/bin

RUN apk add --no-cache maven && \
    curl -LOk https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip && \
    unzip gradle-${GRADLE_VERSION}-bin.zip -d /opt && \
    rm -f gradle-${GRADLE_VERSION}-bin.zip && \
    ln -s /opt/gradle-${GRADLE_VERSION} /opt/gradle && \
    mkdir -p $HOME/.m2 && \
    mkdir -p $HOME/.gradle

ADD bin/bin/scl_enable /usr/local/bin/scl_enable
ADD bin/bin/configure-slave /usr/local/bin/configure-slave
ADD bin/settings.xml $HOME/.m2/
ADD bin/init.gradle $HOME/.gradle/

ENV GOLANG_VERSION=1.10.2

COPY *.patch /go-alpine-patches/

RUN set -eux; \
	apk add --no-cache --virtual .build-deps \
		bash \
		gcc \
		musl-dev \
		openssl \
		go \
	; \
	export \
# set GOROOT_BOOTSTRAP such that we can actually build Go
		GOROOT_BOOTSTRAP="$(go env GOROOT)" \
# ... and set "cross-building" related vars to the installed system's values so that we create a build targeting the proper arch
# (for example, if our build host is GOARCH=amd64, but our build env/image is GOARCH=386, our build needs GOARCH=386)
		GOOS="$(go env GOOS)" \
		GOARCH="$(go env GOARCH)" \
		GOHOSTOS="$(go env GOHOSTOS)" \
		GOHOSTARCH="$(go env GOHOSTARCH)" \
	; \
# also explicitly set GO386 and GOARM if appropriate
# https://github.com/docker-library/golang/issues/184
	apkArch="$(apk --print-arch)"; \
	case "$apkArch" in \
		armhf) export GOARM='6' ;; \
		x86) export GO386='387' ;; \
	esac; \
	\
	wget -O go.tgz "https://golang.org/dl/go$GOLANG_VERSION.src.tar.gz"; \
	echo '6264609c6b9cd8ed8e02ca84605d727ce1898d74efa79841660b2e3e985a98bd *go.tgz' | sha256sum -c -; \
	tar -C /usr/local -xzf go.tgz; \
	rm go.tgz; \
	\
	cd /usr/local/go/src; \
	for p in /go-alpine-patches/*.patch; do \
		[ -f "$p" ] || continue; \
		patch -p2 -i "$p"; \
	done; \
	./make.bash; \
	\
	rm -rf /go-alpine-patches; \
	apk del .build-deps; \
	\
	export PATH="/usr/local/go/bin:$PATH"; \
	go version

ENV GOPATH=/go \
    GOROOT=/usr/local/go \
    PATH=/usr/local/go/bin:/go/bin:$PATH

RUN mkdir -p $GOPATH/src $GOPATH/bin && chmod -R 777 $GOPATH && \
    go get -u github.com/golang/dep/cmd/dep
    #go get -u github.com/golang/lint/golint && \
    #go get github.com/tebeka/go2xunit && \
    #go get -u gopkg.in/alecthomas/gometalinter.v2 && \
    #gometalinter.v2 --install && \
    #go get -u github.com/axw/gocov/... && \
    #go get -u github.com/AlekSi/gocov-xml && \
    #go get -u github.com/jstemmer/go-junit-report && \
    #go get -u github.com/openshift/source-to-image/cmd/s2i && \
    #rm -rf $GOPATH/src/*

# NODEJS PART
RUN apk add --no-cache nodejs yarn python openssh && \
    npm install -g @angular/cli --unsafe-perm

# SONAR PART
ENV TERM=xterm
ENV LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8 \
    LC_CTYPE=en_US.UTF-8 \
    LC_ALL=en_US.UTF-8

RUN apk --update --no-cache add socat curl tzdata findutils \
    && ln -fs /usr/share/zoneinfo/GMT /etc/localtime

ENV SCANNER_VERSION=3.0.3.778
RUN set -e \
    && cd /usr/local \
    && wget https://sonarsource.bintray.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-${SCANNER_VERSION}-linux.zip \
    && unzip sonar-scanner-cli-${SCANNER_VERSION}-linux.zip \
    && rm sonar-scanner-cli-${SCANNER_VERSION}-linux.zip \
    && ln -s /usr/local/sonar-scanner-${SCANNER_VERSION}-linux/bin/sonar-scanner /usr/local/bin/sonar-scanner \
    && ln -s /usr/local/sonar-scanner-${SCANNER_VERSION}-linux/bin/sonar-scanner-debug /usr/local/bin/sonar-scanner-debug \
    && mkdir -p /var/log/mathilde
ADD cmdsonar /usr/local/bin/

RUN chmod -R g+rw $HOME

# run the Jenkins JNLP client
ENTRYPOINT ["/usr/local/bin/run-jnlp-client"]
@Library('alauda-cicd') _

def RELEASE_VERSION = ""

pipeline {
    // lets use the docker agent
    agent {
        node 'static'
    }

    environment {
        JENKINS_VERSION = "2.126"
        GOLANG_VERSION = "1.9.3"

        REGISTRY = "index.alauda.cn"
        REGISTRY_CREDENTIALS = "alaudacn-daniel"

        REPOSITORY = "alaudaorg/jenkins"
        ALAUDA_REPO = "alaudaorg/alauda-jenkins"
        GO_SLAVE_REPO = "alaudaorg/jenkins-node-golang"
        NODEJS_SLAVE_REPO = "alaudaorg/jenkins-node-nodejs"
        JAVA_SLAVE_REPO = "alaudaorg/jenkins-node-java"
        PYTHON_SLAVE_REPO = "alaudaorg/jenkins-node-python"
        ALL_SLAVE_REPO = "alaudaorg/jenkins-node-all"
        ALL_ALPINE_SLAVE_REPO = "alaudaorg/jenkins-node-all-alpine"
        CHROME_SLAVE_REPO = "alaudaorg/jenkins-node-chrome"
        SONAR_SLAVE_REPO = "alaudaorg/jenkins-node-sonar"
        SONAR_SCANNER_JENKINS_REPO = "alaudaorg/jenkins-sonar-scanner"

        PROD_TAG = "latest"

        MASTER_TAG = "master-2.126"

        REGISTRY_URL= "https://${REGISTRY}"
        REPOSITORY_FULL = "${REGISTRY}/${REPOSITORY}"
        ALAUDA_REPO_FULL = "${REGISTRY}/${ALAUDA_REPO}"
        GO_SLAVE_REPO_FULL = "${REGISTRY}/${GO_SLAVE_REPO}"
        NODEJS_SLAVE_REPO_FULL = "${REGISTRY}/${NODEJS_SLAVE_REPO}"
        JAVA_SLAVE_REPO_FULL = "${REGISTRY}/${JAVA_SLAVE_REPO}"
        PYTHON_SLAVE_REPO_FULL = "${REGISTRY}/${PYTHON_SLAVE_REPO}"
        ALL_SLAVE_REPO_FULL = "${REGISTRY}/${ALL_SLAVE_REPO}"
        ALL_ALPINE_SLAVE_REPO_FULL = "${REGISTRY}/${ALL_ALPINE_SLAVE_REPO}"
        CHROME_SLAVE_REPO_FULL = "${REGISTRY}/${CHROME_SLAVE_REPO}"
        SONAR_SLAVE_REPO_FULL = "${REGISTRY}/${SONAR_SLAVE_REPO}"
        SONAR_SCANNER_JENKINS_REPO_FULL = "${REGISTRY}/${SONAR_SCANNER_JENKINS_REPO}"

        NAME = "Jenkins"
        DINGDING_BOT = "ding-jenkins-webhook"
    }
    options {
        skipDefaultCheckout()
    }
    stages {
        stage('Checkout') {
            steps {
                script {
                    def scmVars = checkout scm
                    env.GIT_COMMIT = scmVars.GIT_COMMIT
                    COMMIT_ID = scmVars.GIT_COMMIT // COMMIT_ID is Groovy object
                    env.GIT_BRANCH = scmVars.GIT_BRANCH

                    // double quotation will be expand as Groovy object
                    // single quotation will not be expand, so , it will be treat as environments
                    RELEASE_VERSION_CURRENT = readFile('release.version').trim()
                    echo "git commit \"${env.GIT_COMMIT}\" branch \"${env.GIT_BRANCH}\" release.version \"${RELEASE_VERSION_CURRENT}\" "
                }
            }
        }

        stage('Build go') {
            failFast true
            parallel{
                stage("Build generator") {
                    steps {
                        script {
                            sh 'go version'
                            sh 'mkdir -p $GOPATH/src/devops/jenkins/generator'
                            sh 'cp -R generator/* $GOPATH/src/devops/jenkins/generator'
                            sh 'cp -R generator/vendor/* $GOPATH/src/'
                            sh 'go build -ldflags "-w -s" -v -o gen-go devops/jenkins/generator'
                            sh 'ls -la'
                            stash 'gen-go'
                            sh 'go get -u github.com/alauda/gitversion'

                            if (env.GIT_BRANCH ==~ /release.*/) {
                                RELEASE_VERSION = sh(script:"gitversion patch ${RELEASE_VERSION_CURRENT}", returnStdout: true).trim()
                                echo "RELEASE_VERSION is ${RELEASE_VERSION}"
                            }
                        }
                    }
                }
                stage("Build cmdsonar"){
                    steps {
                        script {
                            sh "git clone --depth=1 https://github.com/alauda/bergamot.git \
                                $GOPATH/src/github.com/alauda/bergamot"
                            sh "CGO_ENABLED=0 go build -a -installsuffix cgo -o cmdsonar \
                                github.com/alauda/bergamot/sonarqube/cmdclient"
                            stash includes: 'cmdsonar', name: 'cmdsonar-binary'
                        }
                    }
                }
            }
        }

        stage('Build Bases') {
            failFast true
            parallel {
                stage('Master base') {
                    steps {
                        // builds the base image
                        // no need to push
                        unstash 'gen-go'
                        // we should copy jenkins.yaml as default configuration
                        sh "cp gen-go base/bin/utils/generator && cp generator/assets/config.yaml base/bin/jenkins.yaml"
                        sh "docker build -t ${REPOSITORY_FULL}:base --build-arg jenkins_version=${JENKINS_VERSION} -f base/Dockerfile base"
                    }
                }
                stage ('Slave base') {
                    steps {
                        script {
                            sh "docker build -t ${REPOSITORY_FULL}:node --build-arg commit_id=${COMMIT_ID} -f slave-base/Dockerfile slave-base"
                        }
                    }
                }
            }
        }
        stage("Build master base"){
            steps {
                script {
                    sh "docker build -t ${REPOSITORY_FULL}:${MASTER_TAG} -f master/Dockerfile master"
                    sh "docker tag ${REPOSITORY_FULL}:${MASTER_TAG} ${REPOSITORY_FULL}:master"
                }
            }
        }

        stage('Build final images') {
            parallel {
                stage('Alauda master') {
                    environment {
                        TOKEN = credentials('alaudaorg-token')
                    }
                    steps {
                        script {
                            def pluginVersion = "release"
                            if (env.GIT_BRANCH == "master"){
                                pluginVersion = "master"
                            }
                            sh "curl -X GET http://oss.alauda.cn/alaudaorg/global/alauda-plugin/${pluginVersion}  -H 'Authorization: Token '${TOKEN} -H 'Content-Type: application/octet-stream' -o master-alauda/alauda.hpi"
                            sh "docker build -t ${ALAUDA_REPO_FULL}:${COMMIT_ID} -t ${ALAUDA_REPO_FULL}:latest --build-arg commit_id=${COMMIT_ID} -f master-alauda/Dockerfile master-alauda"
                        }
                    }
                }
                stage('Slave java') {
                    steps {
                        script {
                            sh "docker build -t ${JAVA_SLAVE_REPO_FULL}:${COMMIT_ID} -t ${JAVA_SLAVE_REPO_FULL}:latest --build-arg commit_id=${COMMIT_ID} -f slave-java/Dockerfile slave-java"
                        }
                    }
                }
                stage('Slave python') {
                    steps {
                        script {
                            sh "docker build -t ${PYTHON_SLAVE_REPO_FULL}:${COMMIT_ID} -t ${PYTHON_SLAVE_REPO_FULL}:latest --build-arg commit_id=${COMMIT_ID} -f slave-python/Dockerfile slave-python"
                        }
                    }
                }
                stage('Slave go') {
                    steps {
                        script {
                            sh "docker build -t ${GO_SLAVE_REPO_FULL}:${COMMIT_ID} -t ${GO_SLAVE_REPO_FULL}:latest --build-arg commit_id=${COMMIT_ID} --build-arg go_version=${GOLANG_VERSION} -f slave-go/Dockerfile slave-go"
                        }
                    }
                }
                stage('Slave nodejs') {
                    steps {
                        script {
                            sh "docker build -t ${NODEJS_SLAVE_REPO_FULL}:${COMMIT_ID} -t ${NODEJS_SLAVE_REPO_FULL}:latest --build-arg commit_id=${COMMIT_ID} -f slave-nodejs/Dockerfile slave-nodejs"
                            sh "docker tag ${NODEJS_SLAVE_REPO_FULL}:${COMMIT_ID} index.alauda.cn/alaudaorg/jenkins-node-nodejs:node"
                        }
                    }
                }
                stage('Slave sonar') {
                    steps {
                        script {
                            unstash 'cmdsonar-binary'
                            sh "cp cmdsonar slave-sonar/"
                            sh "docker build -t ${SONAR_SLAVE_REPO_FULL}:${COMMIT_ID} -t ${SONAR_SLAVE_REPO_FULL}:latest --build-arg commit_id=${COMMIT_ID} -f slave-sonar/Dockerfile slave-sonar"
                        }
                    }
                }
                stage('Slave standalone sonar') {
                    steps {
                        script {
                            unstash 'cmdsonar-binary'
                            sh "cp cmdsonar slave-sonar/"
                            sh "docker build -t ${SONAR_SCANNER_JENKINS_REPO_FULL}:${COMMIT_ID} -t ${SONAR_SCANNER_JENKINS_REPO_FULL}:latest --build-arg commit_id=${COMMIT_ID} -f slave-sonar/Dockerfile.standalone slave-sonar"
                        }
                    }
                }
                stage('Slave all-in-one') {
                    steps {
                        script {
                            // copying files from java
                            unstash 'cmdsonar-binary'
                            sh "cp cmdsonar slave-all-in/"
                            sh "chmod +x ./slave-all-in/copy.sh && ./slave-all-in/copy.sh"
                            sh "docker build -t ${ALL_SLAVE_REPO_FULL}:${COMMIT_ID} -t ${ALL_SLAVE_REPO_FULL}:latest --build-arg commit_id=${COMMIT_ID} --build-arg go_version=${GOLANG_VERSION} -f slave-all-in/Dockerfile slave-all-in"
                        }
                    }
                }
                stage('Slave all-in-one-alpine') {
                    steps {
                        script {
                            // copying files from java
                            unstash 'cmdsonar-binary'
                            sh "cp cmdsonar slave-base/"
                            sh "cp -R slave-java/bin slave-base"
                            sh "docker build -t ${ALL_ALPINE_SLAVE_REPO_FULL}:${COMMIT_ID} -t ${ALL_ALPINE_SLAVE_REPO_FULL}:latest -f slave-base/Dockerfile.alpine slave-base"
                        }
                    }
                }
            }
        }

        stage('Build chrome slave') {
            steps {
                script {
                    sh "docker build -t ${CHROME_SLAVE_REPO_FULL}:${COMMIT_ID} -t ${CHROME_SLAVE_REPO_FULL}:latest -f slave-chrome/Dockerfile slave-chrome"
                }
            }
        }

        stage('Push images') {
            // in the future
            // when {
            //     branch 'release'
            // }
            
            steps {
                script {
                    echo "skip"
                    // withCredentials([usernamePassword(credentialsId: 'dockerhub-danielfbm', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                    //     sh 'docker login -u $USERNAME -p $PASSWORD'
                        /*
                        sh """
                            docker tag ${REPOSITORY_FULL}:master danielfbm/jenk:master
                            docker tag ${REPOSITORY_FULL}:node danielfbm/jenk:node
                            docker tag ${ALAUDA_REPO_FULL}:${COMMIT_ID} danielfbm/jenk-alauda:${COMMIT_ID}
                            docker tag ${GO_SLAVE_REPO_FULL}:${COMMIT_ID} danielfbm/jenk-node-golang:${COMMIT_ID}
                            docker tag ${NODEJS_SLAVE_REPO_FULL}:${COMMIT_ID} danielfbm/jenk-node-nodejs:${COMMIT_ID}
                            docker tag ${PYTHON_SLAVE_REPO_FULL}:${COMMIT_ID} danielfbm/jenk-node-python:${COMMIT_ID}
                            docker tag ${JAVA_SLAVE_REPO_FULL}:${COMMIT_ID} danielfbm/jenk-node-java:${COMMIT_ID}
                            docker tag ${ALL_SLAVE_REPO_FULL}:${COMMIT_ID} danielfbm/jenk-node-all:${COMMIT_ID}
                            docker tag ${ALL_ALPINE_SLAVE_REPO_FULL}:${COMMIT_ID} danielfbm/jenk-node-all-alpine:${COMMIT_ID}
                            docker tag ${CHROME_SLAVE_REPO_FULL}:${COMMIT_ID} danielfbm/jenk-node-chrome:${COMMIT_ID}
                            docker tag ${SONAR_SLAVE_REPO_FULL}:${COMMIT_ID} danielfbm/jenk-node-sonar:${COMMIT_ID}
                        """
                        */
                        
                        /*
                        sh "docker push danielfbm/jenk:master"
                        sh "docker push danielfbm/jenk:node"
                        sh "docker push danielfbm/jenk-alauda:${COMMIT_ID}"
                        sh "docker push danielfbm/jenk-node-java:${COMMIT_ID}"
                        sh "docker push danielfbm/jenk-node-python:${COMMIT_ID}"
                        sh "docker push danielfbm/jenk-node-golang:${COMMIT_ID}"
                        sh "docker push danielfbm/jenk-node-nodejs:${COMMIT_ID}"
                        sh "docker push danielfbm/jenk-node-all:${COMMIT_ID}"
                        sh "docker push danielfbm/jenk-node-all-alpine:${COMMIT_ID}"
                        sh "docker push danielfbm/jenk-node-chrome:${COMMIT_ID}"
                        sh "docker push danielfbm/jenk-node-sonar:${COMMIT_ID}"
                        */

                        // dev tag
                        /*
                        sh "docker tag danielfbm/jenk-node-java:${COMMIT_ID} danielfbm/jenk-node-java:dev && docker push danielfbm/jenk-node-java:dev"
                        sh "docker tag danielfbm/jenk-node-python:${COMMIT_ID} danielfbm/jenk-node-python:dev && docker push danielfbm/jenk-node-python:dev"
                        sh "docker tag danielfbm/jenk-node-golang:${COMMIT_ID} danielfbm/jenk-node-golang:dev && docker push danielfbm/jenk-node-golang:dev"
                        sh "docker tag danielfbm/jenk-node-nodejs:${COMMIT_ID} danielfbm/jenk-node-nodejs:dev && docker push danielfbm/jenk-node-nodejs:dev"
                        sh "docker tag danielfbm/jenk-node-all:${COMMIT_ID} danielfbm/jenk-node-all:dev && docker push danielfbm/jenk-node-all:dev"
                        sh "docker tag danielfbm/jenk-node-all-alpine:${COMMIT_ID} danielfbm/jenk-node-all-alpine:dev && docker push danielfbm/jenk-node-all-alpine:dev"
                        sh "docker tag danielfbm/jenk-node-chrome:${COMMIT_ID} danielfbm/jenk-node-chrome:dev && docker push danielfbm/jenk-node-chrome:dev"
                        sh "docker tag danielfbm/jenk-node-sonar:${COMMIT_ID} danielfbm/jenk-node-sonar:dev && docker push danielfbm/jenk-node-sonar:dev"
                        */
                    // }
                }

                script {
                    withCredentials([usernamePassword(credentialsId: 'alaudacn-daniel', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                        sh 'docker login ${REGISTRY} -u $USERNAME -p $PASSWORD'

                        sh "docker push ${REPOSITORY_FULL}:base"
                        sh "docker push ${REPOSITORY_FULL}:${MASTER_TAG}"
                        sh "docker push ${REPOSITORY_FULL}:node"

                        sh "docker push ${ALAUDA_REPO_FULL}:${COMMIT_ID}"
                        sh "docker push ${JAVA_SLAVE_REPO_FULL}:${COMMIT_ID}"
                        sh "docker push ${PYTHON_SLAVE_REPO_FULL}:${COMMIT_ID}"
                        sh "docker push ${GO_SLAVE_REPO_FULL}:${COMMIT_ID}"
                        sh "docker push ${NODEJS_SLAVE_REPO_FULL}:${COMMIT_ID}"
                        sh "docker push ${ALL_SLAVE_REPO_FULL}:${COMMIT_ID}"
                        sh "docker push ${CHROME_SLAVE_REPO_FULL}:${COMMIT_ID}"
                        sh "docker push ${SONAR_SLAVE_REPO_FULL}:${COMMIT_ID}"
                        sh "docker push ${SONAR_SCANNER_JENKINS_REPO_FULL}:${COMMIT_ID}"
                        sh "docker push ${ALL_ALPINE_SLAVE_REPO_FULL}:${COMMIT_ID}"

                        sh "docker push ${ALAUDA_REPO_FULL}:latest"
                        sh "docker push ${JAVA_SLAVE_REPO_FULL}:latest"
                        sh "docker push ${PYTHON_SLAVE_REPO_FULL}:latest"
                        sh "docker push ${GO_SLAVE_REPO_FULL}:latest"
                        sh "docker push ${NODEJS_SLAVE_REPO_FULL}:latest"
                        sh "docker push ${ALL_SLAVE_REPO_FULL}:latest"
                        sh "docker push ${ALL_ALPINE_SLAVE_REPO_FULL}:latest"
                        sh "docker push ${CHROME_SLAVE_REPO_FULL}:latest"
                        sh "docker push ${SONAR_SLAVE_REPO_FULL}:latest"
                        sh "docker push ${SONAR_SCANNER_JENKINS_REPO_FULL}:latest"
                    }
                }
            }
        }

        stage('Release image'){
            when {
                expression{
                    BRANCH_NAME ==~ /release.*/ && RELEASE_VERSION != ""
                }
            }
            steps{
                script {
                    sh "docker tag ${ALAUDA_REPO_FULL}:${COMMIT_ID} ${ALAUDA_REPO_FULL}:${RELEASE_VERSION}"
                    sh "docker tag ${JAVA_SLAVE_REPO_FULL}:${COMMIT_ID} ${JAVA_SLAVE_REPO_FULL}:${RELEASE_VERSION}"
                    sh "docker tag ${PYTHON_SLAVE_REPO_FULL}:${COMMIT_ID} ${PYTHON_SLAVE_REPO_FULL}:${RELEASE_VERSION}"
                    sh "docker tag ${GO_SLAVE_REPO_FULL}:${COMMIT_ID} ${GO_SLAVE_REPO_FULL}:${RELEASE_VERSION}"
                    sh "docker tag ${NODEJS_SLAVE_REPO_FULL}:${COMMIT_ID} ${NODEJS_SLAVE_REPO_FULL}:${RELEASE_VERSION}"
                    sh "docker tag ${ALL_SLAVE_REPO_FULL}:${COMMIT_ID} ${ALL_SLAVE_REPO_FULL}:${RELEASE_VERSION}"
                    sh "docker tag ${ALL_ALPINE_SLAVE_REPO_FULL}:${COMMIT_ID} ${ALL_ALPINE_SLAVE_REPO_FULL}:${RELEASE_VERSION}"
                    sh "docker tag ${CHROME_SLAVE_REPO_FULL}:${COMMIT_ID} ${CHROME_SLAVE_REPO_FULL}:${RELEASE_VERSION}"
                    sh "docker tag ${SONAR_SLAVE_REPO_FULL}:${COMMIT_ID} ${SONAR_SLAVE_REPO_FULL}:${RELEASE_VERSION}"
                    sh "docker tag ${SONAR_SCANNER_JENKINS_REPO_FULL}:${COMMIT_ID} ${SONAR_SCANNER_JENKINS_REPO_FULL}:${RELEASE_VERSION}"
                }
                script {
                    withCredentials([usernamePassword(credentialsId: 'alaudacn-daniel', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                        sh 'docker login ${REGISTRY} -u $USERNAME -p $PASSWORD'

                        sh "docker push ${ALAUDA_REPO_FULL}:${RELEASE_VERSION}"
                        sh "docker push ${JAVA_SLAVE_REPO_FULL}:${RELEASE_VERSION}"
                        sh "docker push ${PYTHON_SLAVE_REPO_FULL}:${RELEASE_VERSION}"
                        sh "docker push ${GO_SLAVE_REPO_FULL}:${RELEASE_VERSION}"
                        sh "docker push ${NODEJS_SLAVE_REPO_FULL}:${RELEASE_VERSION}"
                        sh "docker push ${ALL_SLAVE_REPO_FULL}:${RELEASE_VERSION}"
                        sh "docker push ${ALL_ALPINE_SLAVE_REPO_FULL}:${RELEASE_VERSION}"
                        sh "docker push ${CHROME_SLAVE_REPO_FULL}:${RELEASE_VERSION}"
                        sh "docker push ${SONAR_SLAVE_REPO_FULL}:${RELEASE_VERSION}"
                        sh "docker push ${SONAR_SCANNER_JENKINS_REPO_FULL}:${RELEASE_VERSION}"
                    }
                }
            }
        }

        stage('Tag Git Repo'){
            when {
                expression{
                    BRANCH_NAME ==~ /release.*/ && RELEASE_VERSION != ""
                }
            }
            steps{
                script{
                    withCredentials([usernamePassword(credentialsId: 'bitbucket', passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
                        sh "git tag -l | xargs git tag -d" // clean local tags
                        def repo = "https://${GIT_USERNAME}:${GIT_PASSWORD}@bitbucket.org/mathildetech/jenkins.git"
                        sh "git fetch --tags ${repo}" // retrieve all tags
                        sh("git tag -a ${RELEASE_VERSION} -m 'auto add release tag by jenkins'")
                        sh("git push ${repo} --tags")
                    }
                }
            }
        }
    }

    post {
        // 成功
        success {
            script {
                deploy.notificationSuccess("${NAME}", DINGDING_BOT, "流水线完成了", "${GIT_BRANCH} ${GIT_COMMIT} ${RELEASE_VERSION}")
            }
        }
        // 失败
        failure {
            script{
                deploy.notificationFailed("${NAME}", DINGDING_BOT, "流水线失败了", "${GIT_BRANCH} ${GIT_COMMIT} ${RELEASE_VERSION}")
            }
        }
        // 取消的
        aborted {
            echo "aborted!"
        }
    }
}